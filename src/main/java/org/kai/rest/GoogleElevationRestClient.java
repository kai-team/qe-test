package org.kai.rest;

import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import org.kai.rest.json.ElevationResponse;
import org.kai.rest.json.Results;

/**
 * TODO:
 */
public class GoogleElevationRestClient {

    private static final Logger log = LogManager.getLogger("GoogleElevationRestClient");

    private static final String ELEVATION_URL = "https://maps.googleapis.com/maps/api/elevation/json?locations=";

    public int getElevation(String latitude, String longitude) {

        int elevation;

        log.trace("getElevation(latitude: {}, longitude: {}", latitude, longitude);

        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(ELEVATION_URL + latitude + "," + longitude);

            CloseableHttpResponse response = httpClient.execute(httpGet);
            log.trace("HTTP Response: {}", response);

            if (response.getStatusLine().getStatusCode() != 200)
                throw new RuntimeException("HTTP Error: " + response.getStatusLine().getStatusCode());

            try {
                HttpEntity entity = response.getEntity();
                if (entity == null)
                    throw new RuntimeException("HTTP response has no content.");

                log.trace("HTTP Entity: {}", entity);

                Reader reader = new InputStreamReader(entity.getContent(), ContentType.getOrDefault(entity).getCharset());
                ElevationResponse json = new GsonBuilder().create().fromJson(reader, ElevationResponse.class);
                Results jsonResult = json.results.get(0);

                elevation = (int)Double.parseDouble(jsonResult.elevation);

                log.trace("Lat: {} Lng: {} Elevation: {}", jsonResult.location.lat, jsonResult.location.lng, jsonResult.elevation);

            } finally {
                response.close();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return elevation;
    }

}
