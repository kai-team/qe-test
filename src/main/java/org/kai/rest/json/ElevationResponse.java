package org.kai.rest.json;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO:
 */
public class ElevationResponse {
    public List<Results> results = new ArrayList<Results>();
    public String status;
}

/*
    {
       "results" : [
          {
             "elevation" : 1608.637939453125,
             "location" : {
                "lat" : 39.7391536,
                "lng" : -104.9847034
             },
             "resolution" : 4.771975994110107
          }
       ],
       "status" : "OK"
    }
 */