package org.kai.rest;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GoogleElevationRestClientTest {

    private static final String LATITUDE = "39.73915360";
    private static final String LONGITUDE = "-104.98470340";
    private static final int ELEVATION = 1608;

    @Test
    public void smoke() {
        GoogleElevationRestClient client = new GoogleElevationRestClient();

        Assert.assertEquals("", ELEVATION, client.getElevation(LATITUDE, LONGITUDE));
    }
}